﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoDataBase
    {
         public int Salvar (PedidoDTO dto)
        {
            String script = @"INSERT INTO tb_pedido (nm_cliente. ds_cpf, dt_venda) VALUES (nm_cliente, @ds_cpf, @dt_venda)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Cliente));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("dt_venda", dto.Data));

            PedidoItemDatabase db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

    }
}
